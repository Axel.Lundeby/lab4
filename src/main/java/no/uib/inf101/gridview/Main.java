package no.uib.inf101.gridview;


import javax.swing.JFrame;

import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.*;
import no.uib.inf101.colorgrid.CellPosition;

//jobbet med Victor Slorer
public class Main {
  public static void main(String[] args) {
   

    JFrame frame = new JFrame();//
    
    IColorGrid grid = new ColorGrid(3, 4);//ant rows og cols
    grid.set(new CellPosition(0, 0), Color.RED);//spesifike celler farges
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    
    GridView gv = new GridView(grid);//oppretter et grid viewet
    frame.setTitle("INF101 Axel Lundeby");
    frame.setContentPane(gv);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}


