package no.uib.inf101.gridview;
import java.awt.Dimension;
import java.awt.Color;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.geom.Rectangle2D;
import java.util.List;
import java.awt.Graphics2D;
import java.awt.Graphics;

public class GridView extends JPanel{
    IColorGrid iColorGrid;
    private static final double OUTERMARGIN = 30;//konstant variabel avstand fra kantan
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;//fargen til lerretet

    public GridView(IColorGrid iColorGrid){
      this.iColorGrid=iColorGrid;//felt variabel
      this.setPreferredSize(new Dimension(400, 300));//størrelsen på lerretet
    }


  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
    }

    public void drawGrid(Graphics2D g2){
        double x = OUTERMARGIN;//x starter på margin
        double y = OUTERMARGIN;//y starter på margin

        double width= this.getWidth() - 2 * OUTERMARGIN;//bredde
        double height= this.getHeight() - 2 * OUTERMARGIN;//høyde

        Rectangle2D backround = new Rectangle2D.Double(x,y,width,height);
        g2.setColor(MARGINCOLOR);
        g2.fill(backround);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(backround, iColorGrid,OUTERMARGIN);//
        drawCells(g2, iColorGrid, converter);//kall til draw cells
       }
          
        
      private static void drawCells(Graphics2D g2, CellColorCollection colors, CellPositionToPixelConverter converter){
      List<CellColor> list = colors.getCells();//
      for (CellColor cell : list) {//løkke itererer gjennom cellene
        Rectangle2D rectangle = converter.getBoundsForCell(cell.cellPosition());//kaller på en metode i cellpotition
        Color color = cell.color();
        if (cell.color() == null){//om det ikke er nevnt en farge i mainen
        color = Color.DARK_GRAY;//skal fargen være grå
      }
      g2.setColor(color);//gir cellene farge
      g2.fill(rectangle);//fargelegger de(jeg kan se de)
    }
    }
}

