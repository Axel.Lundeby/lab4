package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
    int rows;
    int cols;
    List<List<Color>> grid; 


  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();

    for (int i = 0; i < rows; i++) {//itererer gjennom cols
      this.grid.add(new ArrayList<>());//legger til liste i gridet
      for (int j = 0; j < cols; j++) {//itererer gjennom cols
        List<Color> row = this.grid.get(i);// legger til rows i listen
        row.add(null);//legger til ting i listen over
      }
    }
  }


  @Override
  public int rows() {//denne metoden vil returnere antall rader
    return this.rows;
  }


  @Override
  public int cols() {//denne metoden vil returnere antall cols
    return this.cols;
  }


  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();//ny liste
    for (int i = 0; i < rows; i++) {//itererer gjennom rows
      for (int j = 0; j < cols; j++) {//itererer gjennom cols
        CellPosition pos = new CellPosition(i, j);//kordinatet til en celle
        Color color = get(pos);//fargen er indeksen til posisjonen
        cells.add(new CellColor(pos, color));//legger til posisjonen og fargen i cellcolor i liten vi lagde
        }
      }
    return cells;//returnerer listen
  }


  @Override
  public Color get(CellPosition pos) {
    return this.grid.get(pos.row()).get(pos.col());//returnerer fargen til cellen
  }

  @Override
  public void set(CellPosition pos, Color color) {
   this.grid.get(pos.row()).set(pos.col(),color);//farger cellene på en gitt posisjon
  }
}
