package no.uib.inf101.bonus;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.gridview.GridView;

public class Main {
  public static void main(String[] args) {
    // Kopier inn main-metoden fra kursnotatene om grafikk her,
    // men tilpass den slik at du oppretter et BeautifulPicture -objekt
    // som lerret.

    GridView gv = new GridView(null);
    JFrame frame = new JFrame();
    
    frame.setContentPane(gv);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
